// console.log('hello')

let nav_btn = document.querySelector('.header__navbar-btn .header__navbar-icon')
let nav_menu_mobile = document.querySelector('.header__navbar--right')
let btn_exit_toggler = document.querySelector('.navbar-toggler')

nav_btn.addEventListener('click', () => {
    nav_menu_mobile.classList.add('show');
});

btn_exit_toggler.addEventListener('click', () => {
    const isMenuBarShowed = nav_menu_mobile.classList.contains('show')

    if (isMenuBarShowed) {
        nav_menu_mobile.classList.remove('show')
    }
    else {
        nav_menu_mobile.classList.add('show')
    }
})